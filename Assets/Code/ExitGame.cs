﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitGame : MonoBehaviour {
    public string Test { get; private set; }

    public void MENU_LEAVE()
    {
        Application.Quit();

    }
}
