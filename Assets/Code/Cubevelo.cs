﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cubevelo : MonoBehaviour {

    public Rigidbody swanky;
    public int force = 12;

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().AddForce(transform.up * force);
        GetComponent<Rigidbody>().AddForce(transform.forward * force);
    }
	
	// Update is called once per frame
	void Update () {
       // GetComponent<Rigidbody>().AddForce(transform.up * force);
      //  GetComponent<Rigidbody>().AddForce(transform.forward * force);
    }

}
