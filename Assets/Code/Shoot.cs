﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour
{

    public Rigidbody swanky;
    public Rigidbody baggete;
    public Rigidbody George;
    public GameObject Gun;
    public Transform spawn;
    public Transform bspawn;
    public int power = 150;
    public AudioClip boi;
    public Text ScoreText;
    private static int ammoCount = 0;
    public int gun = 1;
    public int Progress = 2;


    public GameObject GunSmoke { get; private set; }

    // Use this for initialization
    void Start()
    {
        //swanky = GetComponent<Rigidbody>();
    }

    IEnumerator waiting()
    {
        Gun.transform.Translate(Vector3.left * 0.05f);
        yield return new WaitForSeconds(0.1f);
        Gun.transform.Translate(Vector3.right * 0.05f);

    }


    public void Bag()
    {

        if (Input.GetMouseButtonDown(0))
        {
            ammoCount += 1;
           
                power = 400;
                Instantiate(baggete, bspawn.position, bspawn.rotation);
                swanky.velocity = transform.TransformDirection(Vector3.forward * 500);
                swanky.AddForce(transform.forward * power);
            //RECOIL\\
            StartCoroutine("waiting");
        }
    }

        // Update is called once per frame
        void Update()
    {
        string AmmoCountString = ammoCount.ToString();

    ScoreText.text = "gun: " + gun;

        if (gun > 2)
        {
            gun = 1;
        }

        if (Input.GetKeyDown(KeyCode.G) && Progress > 1)
        {
            gun += 1;
        }

        if (gun == 2)
        {
            Bag();
        }

        
        else
        {
            if (Input.GetMouseButtonDown(0))
            {

                ammoCount += 1;

                Debug.Log("Pressed left click.");
                //ass
                if (ammoCount == 69)
                {
                    Instantiate(George, spawn.position, spawn.rotation);
                    George.velocity = transform.TransformDirection(Vector3.forward * 1);
                    George.AddForce(transform.forward * power);

                }
                else
                {
                    Instantiate(swanky, spawn.position, spawn.rotation);
                    swanky.velocity = transform.TransformDirection(Vector3.forward * 1);
                    swanky.AddForce(transform.forward * power);
                }
                //RECOIL\\
                StartCoroutine("waiting");
                //AUDIO\\
                AudioSource.PlayClipAtPoint(boi, transform.position);
            }

        }

    }

}   
